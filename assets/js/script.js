function Slide(index, title, background, link){
	this.title = title;
	this.background = background;
	this.link = link;
	this.id = "slide-" + index;
}

const Slider = {
	current: 0,
	slides: [],

	initSlider: function(slides){
		let index = 0;
		for(let slide of slides){
			this.slides.push(new Slide(index, slide.title, slide.background, slide.link));
			index++;
		}
		
		this.buildSlider();

		document.getElementById("slider-button-hide").addEventListener("click", Slider.hideSlider);
        document.getElementById("slider-button-prev").addEventListener("click", Slider.prevSlide);
        document.getElementById("slider-button-next").addEventListener("click", Slider.nextSlide);

		document.getElementById("slider-button-autoscroll").addEventListener('click', (event)=>{
			if (event.target.classList.contains('start')){
				event.target.innerHTML = 'Stoped';
				clearInterval(interval);
			} else {
				event.target.innerHTML = 'Started';
				interval = setInterval(()=>{
				Slider.nextSlide();
				},1000)
			}
			event.target.classList.toggle('start');
			});
	},

	buildSlider: function(){
		let sliderHTML = "";
		let slidesButtons = document.getElementById("slides-indexes");

		for(let slide of this.slides){
			sliderHTML += `<div id='${slide.id}' class='singleSlide' style='background-image:url(${slide.background});'><div class='slideOverlay'><h2>${slide.title}</h2><a class='link' href='${slide.link}' target='_blank'>Open</a></div></div>`;

			let btn = document.createElement('button');
			btn.className = 'slider-button';
			btn.id = 'button-'+slide.id;
			btn.innerHTML = slide.id;
			btn.addEventListener("click", Slider.setSlide);

			slidesButtons.appendChild(btn);
		}



		document.getElementById("slider").innerHTML = sliderHTML;
		document.getElementById("slide-" + this.current).style.left = 0;
	},

	hideSlider: function(event){
		this.innerHTML = "Unhide";

		document.getElementById("slider").setAttribute("style", "display: none;");
		document.getElementById("slider-button-prev").setAttribute("style", "display: none;");
		document.getElementById("slider-button-next").setAttribute("style", "display: none;");
		document.getElementById("slider-button-autoscroll").setAttribute("style", "display: none;");

		this.removeEventListener("click", Slider.hideSlider);
		this.addEventListener("click", Slider.unhideSlider);
	},

	unhideSlider: function(event){
		this.innerHTML = "Hide";

		document.getElementById("slider").setAttribute("style", "");
		document.getElementById("slider-button-prev").setAttribute("style", "");
		document.getElementById("slider-button-next").setAttribute("style", "");
		document.getElementById("slider-button-autoscroll").setAttribute("style", "");

		this.removeEventListener("click", Slider.unhideSlider);
		this.addEventListener("click", Slider.hideSlider);
	},

	prevSlide: function(event){
		let next;

		if(Slider.current === 0){
			next = Slider.slides.length - 1;
		}
		else{
			next = Slider.current - 1;
		}

		document.getElementById("slide-" + next).style.left = "-100%";
		document.getElementById("slide-" + Slider.current).style.left = 0;

		document.getElementById("slide-" + next).setAttribute("class", "singleSlide slideInLeft");
		document.getElementById("slide-" + Slider.current).setAttribute("class", "singleSlide slideOutRight");

		Slider.current = next;
	},

	nextSlide: function(event){
		let next;

		if(Slider.current === (Slider.slides.length - 1)){
			next = 0;
		}
		else {
			next = Slider.current + 1;
		}

		document.getElementById("slide-" + next).style.left = "100%";
		document.getElementById("slide-" + Slider.current).style.left = 0;

		document.getElementById("slide-" + next).setAttribute("class", "singleSlide slideInRight");
		document.getElementById("slide-" + Slider.current).setAttribute("class", "singleSlide slideOutLeft");
		
		Slider.current = next;
	},

	setSlide: function(event){
		while(this.innerHTML != Slider.slides[Slider.current].id){
			Slider.nextSlide();
		}
	}
}
