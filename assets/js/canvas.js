function canvas(selector, options){
	const canvas = document.querySelector(selector);
	canvas.classList.add('canvas');
	canvas.setAttribute('width', `${options.width || 400}px`);

	const context = canvas.getContext('2d');
	const rect = canvas.getBoundingClientRect();
	const auxPanel = document.getElementById('aux-panel');

	let isPaint = false;
	let points = [];

	const addPoint = (x, y, dragging) => {
		points.push({
			x: (x - rect.left),
			y: (y - rect.top),
			dragging: dragging
		});
	}
	
	const backroundImg = new Image;
	backroundImg.onload = () => { redraw(); }

	const redraw = () => {
		context.clearRect(0, 0, context.canvas.width,
								context.canvas.height);

		context.strokeStyle = options.strokeColor;
		context.lineJoin = "round";
		context.lineWidth = options.strokeWidth;

		context.drawImage(backroundImg, 0, 0);
		let prevPoint = null;

		for(let point of points){
			context.beginPath();
			if(point.dragging && prevPoint){
				context.moveTo(prevPoint.x, prevPoint.y);
			}
			else{
				context.moveTo(point.x - 1, point.y);
			}

			context.lineTo(point.x, point.y);
			context.closePath();
			context.stroke();
			prevPoint = point;
		}
	}

	const mouseDown = envent => {
		isPaint = true;
		addPoint(event.pageX, event.pageY);
		redraw();
	}

	const mouseMove = event => {
		if(isPaint){
			addPoint(event.pageX, event.pageY, true);
			redraw();
		}
	}

	canvas.addEventListener('mousemove', mouseMove);
	canvas.addEventListener('mousedown', mouseDown);
	canvas.addEventListener('mouseup', () => { isPaint = false; });
	canvas.addEventListener('mouseleave', () => { isPaint = false; });

	
	const toolBar = document.getElementById('toolbar');

	const clearBtn = document.createElement('button');
	const downloadBtn = document.createElement('button');
	const saveBtn = document.createElement('button');
	const restoreBtn = document.createElement('button');
	const timestampBtn = document.createElement('button');
	const brushColorBtn = document.createElement('button');
	const brushSizeBtn = document.createElement('button');
	const backgroundBtn = document.createElement('button');

	const buttons = [clearBtn, downloadBtn, saveBtn, restoreBtn, timestampBtn,
					 brushColorBtn, brushSizeBtn, backgroundBtn];

	for(let btn of buttons){
		btn.classList.add('btn');
	}
	
	clearBtn.innerHTML = `<i class="fas fa-broom"></i>`;
	downloadBtn.innerHTML = `<i class="fas fa-download"></i>`;
	saveBtn.innerHTML = `<i class="fas fa-save"></i>`;
	restoreBtn.innerHTML = `<i class="fas fa-window-restore"></i>`;
	timestampBtn.innerHTML = `<i class="fas fa-clock"></i>`;
	brushColorBtn.innerHTML = `<i class="fas fa-palette"></i>`;
	brushSizeBtn.innerHTML = `<i class="fas fa-pencil-ruler"></i>`;
	backgroundBtn.innerHTML = `<i class="fas fa-image"></i>`;

	clearBtn.addEventListener('click', () => {
			points = [];
			context.clearRect(0, 0, context.canvas.width, context.canvas.height);
	});	

	downloadBtn.addEventListener('click', () => {
		const dataUrl = canvas.toDataURL("image/png").replace(/^data:image\/[^;]/,
										 'data:application/octet-stream');
		
		const newTab = window.open('about:blank', 'image from canvas');
		newTab.document.write("<img src='" + dataUrl + "' alt='from canvas' />");
	});

	saveBtn.addEventListener('click', () => {
		localStorage.setItem('points', JSON.stringify(points));

		// blink with check mark
		originHTML = saveBtn.innerHTML;
		saveBtn.innerHTML = `<i class="fas fa-check"></i>`;
		saveBtn.setAttribute('style', ' background-color: DarkGreen; ');
		setTimeout(() => { 
				saveBtn.innerHTML = originHTML;
				saveBtn.setAttribute('style', " background-color: ''");
			}, 500);
	});

	restoreBtn.addEventListener('click', () => {
		points = JSON.parse(localStorage.getItem('points'));
		redraw();
	});

	timestampBtn.addEventListener('click', () => {
		context.fillText((new Date()).toISOString(), 0, 10);
	});


	const brushColorPicker = create_brush_color_input();
	let isPickerActive = false;

	brushColorBtn.addEventListener('click', () => {
		if(isPickerActive){
			brushColorPicker.style.display = "none";
			brushColorBtn.setAttribute('style', '');
			isPickerActive = false;
		}
		else{
			brushColorPicker.style.display = "block";
			brushColorBtn.setAttribute('style', 'background-color: DarkGreen; ');
			isPickerActive = true;
		}

	});


	const brushSizePicker = create_brush_size_input();
	let isSizePickerActive = false;

	brushSizeBtn.addEventListener('click', () => {
		if(isSizePickerActive){
			brushSizePicker.style.display = "none";
			brushSizeBtn.setAttribute('style', '');
			isSizePickerActive = false;
		}
		else{
			brushSizePicker.style.display = "block";
			brushSizeBtn.setAttribute('style', 'background-color: DarkGreen; ');
			isSizePickerActive = true;
		}
	});


	const bgimgUrlInput = create_image_url_input();
	let isBgimgUrlInputActive = false;

	backgroundBtn.addEventListener('click', () => {
		if(isBgimgUrlInputActive){
			bgimgUrlInput.style.display = "none";
			bgimgUrlInput.setAttribute('style', '');
			isBgimgUrlInputActive = false;
		}
		else{
			bgimgUrlInput.style.display = "block";
			backgroundBtn.setAttribute('style', 'background-color: DarkGreen; ');
			isBgimgUrlInputActive = true;
		}
	});

	for(let btn of buttons){
		toolBar.insertAdjacentElement('afterbegin', btn);
	}



	function create_brush_color_input(){
		let input = document.createElement('input');
		input.type = "color";
		input.value = options.strokeColor;

		input.addEventListener('change', () => {
			options.strokeColor = input.value;
			redraw();
		});

		auxPanel.appendChild(input);
		input.style.display = "none";

		return input;
	}

	function create_brush_size_input(){
		let input = document.createElement('input');
		input.type = "range";
		input.setAttribute("min", 1);
		input.setAttribute("max", 12);
		input.value = options.strokeWidth;

		input.addEventListener('change', () => {
			options.strokeWidth = brushSizePicker.value;
			redraw();
		});

		auxPanel.appendChild(input);
		input.style.display = "none";

		return input;
	}

	function create_image_url_input(){
		let input = document.createElement('input');
		input.type = "text";

		input.addEventListener('change', () => {
			if(valid = /^(http|https):\/\/[^ "]+$/.test(input.value))
				backroundImg.crossOrigin = "anonymous";
				backroundImg.src = input.value;
		});

		auxPanel.appendChild(input);
		input.style.display = "none";

		return input;
	}
}


